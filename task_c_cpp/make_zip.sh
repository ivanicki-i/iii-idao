MODEL_NAME=0.80409_v9_c_sqrtw.cbm &&\
ZIP_NAME=7521.47.zip &&\
cp /data/IDAO-MuID/$MODEL_NAME track_2_model.cbm && \
rm -f *.zip && \
rm -rf .ipynb_checkpoints && \
rm -rf __pycache__ && \
touch -d "12 hours ago" ./* && \
zip -r $ZIP_NAME ./ && \
touch -d "5 hours ago" ./* && \
ls -lh .