#include "./parser.h"

inline bool not_number(const char pen) {
    return !isdigit(pen) && !(pen == '.') && !(pen == '-');
}

void skip_to_number_pointer(const char *& pen) {
    while ((*pen) && not_number(*pen)) ++pen;
}

inline float square(const float x) {
    return x*x;
}

// https://stackoverflow.com/questions/5678932/fastest-way-to-read-numerical-values-from-text-file-in-c-double-in-this-case
template<class T>
T rip_uint_pointer(const char *&pen, T val = 0) {
    // Will return val if *pen is not a digit
    // WARNING: no overflow checks
    for (char c; (c = *pen ^ '0') <= 9; ++pen)
        val = val * 10 + c;
    return val;
}

template<class T>
T rip_float_pointer(const char *&pen) {
    static double const exp_lookup[]
        = {1, 0.1, 1e-2, 1e-3, 1e-4, 1e-5, 1e-6, 1e-7, 1e-8, 1e-9, 1e-10,
           1e-11, 1e-12, 1e-13, 1e-14, 1e-15, 1e-16, 1e-17};
    T sign = 1.;
    if (*pen == '-') {
        ++pen;
        sign = -1.;
    }
    uint64_t val = rip_uint_pointer<uint64_t>(pen);
    unsigned int neg_exp = 0;
    if (*pen == '.') {
        const char* const fracs = ++pen;
        val = rip_uint_pointer(pen, val);
        neg_exp  = pen - fracs;
    }
    return std::copysign(val*exp_lookup[neg_exp], sign);
}

// Warning: this is not a general-puropse parser, you have
// std::istream for that. As a rule, in the interest of speed, it
// doesn't check for input correctness and will have undefined
// behavior at incorrect input
class BufferedStream {
 public:
    explicit BufferedStream(std::istream& stream);
    // Discards data from the stream until ecountering a digit, "." or "-"
    void skip_to_number();
    // Reads a float from the stream, starting from the current character
    // and has undefined behaviour if there is no number at the
    // current position
    template<class T> T rip_float() {return rip_float_pointer<T>(pen);}
    // Reads an unsigned integer from stream, starting from the
    // current character and has undefined behaviour if there is no
    // number at the current position
    template<class T> T rip_uint() {return rip_uint_pointer<T>(pen);}
    // Reads a vector of floats of the given size from the stream,
    // skipping everything as needed
    template<class T>
    std::vector<T> fill_vector_float(const size_t size);
    // Reads a vector of unsigned ints of the given size from the stream,
    // skipping as needed. In principle, should be templated from
    // fill_vector_float, but if constexpr is C++17 :(
    template<class T>
    std::vector<T> fill_vector_uint(const size_t size);
    // Reads count floating point numbers and stores them into the
    // container pointed to by the iterator
    template<class IteratorType>
    void fill_iterator_float(const IteratorType& iterator, const size_t count);
    // Discards data from the stream until encountering the delimiter
    void skip_to_char(const char delimiter);
    // Discrads data from the stream until twice encountering the delimiter
    void skip_record(const char delimiter);

 private:
    void next_line();
    // Buffer size is measured to fit the longest line in the test dataset
    // but the code doesn't rely on it
    static const size_t BUFFER_SIZE = 1016;
    char buffer[BUFFER_SIZE];
    std::istream& stream;
    const char* pen;
};

void BufferedStream::next_line() {
    stream.getline(buffer, BUFFER_SIZE);
    pen = buffer;
}

BufferedStream::BufferedStream(std::istream& stream): stream(stream) {
    next_line();
}

void BufferedStream::skip_to_number() {
    skip_to_number_pointer(pen);
    while ((*pen) == 0) {
        next_line();
        skip_to_number_pointer(pen);
        // The skip stops either at 0-byte or
        // a number part
    }
}

template<class T>
std::vector<T> BufferedStream::fill_vector_float(const size_t size) {
    std::vector<T> result(size);
    fill_iterator_float<std::vector<float>::iterator>(result.begin(), size);
    return result;
}

template<class T>
std::vector<T> BufferedStream::fill_vector_uint(const size_t size) {
    std::vector<T> result(size);
    for (auto& value : result) {
        skip_to_number();
        value = rip_uint<T>();
    }
    return result;
}

void BufferedStream::skip_to_char(const char delimiter) {
    while ((*pen) != delimiter) {
        while ((*pen) && (*(++pen)) != delimiter) {}
        if (!(*pen)) next_line();
    }
}

void BufferedStream::skip_record(const char delimiter) {
    skip_to_char(delimiter);
    ++pen;
    skip_to_char(delimiter);
}

template<class IteratorType>
void BufferedStream::fill_iterator_float(const IteratorType& iterator, const size_t count) {
    for (IteratorType value = iterator; value != iterator + count; ++value) {
        skip_to_number();
        *value = rip_float<typename std::iterator_traits<IteratorType>::value_type>();
    }
}

void ugly_hardcoded_parse(std::istream& stream, size_t* id, std::vector<float>* result) {
    BufferedStream buffered_stream(stream);
    *id = buffered_stream.rip_uint<size_t>();
    buffered_stream.fill_iterator_float<std::vector<float>::iterator>(
        result->begin(), N_RAW_FEATURES - N_RAW_FEATURES_TAIL);
    // No need to skip, fill_vector takes care of it
    const size_t FOI_hits_N = (*result)[FOI_HITS_N_INDEX];
    const std::vector<float> FOI_hits_X = buffered_stream.fill_vector_float<float>(FOI_hits_N);
    const std::vector<float> FOI_hits_Y = buffered_stream.fill_vector_float<float>(FOI_hits_N);
    const std::vector<float> FOI_hits_Z = buffered_stream.fill_vector_float<float>(FOI_hits_N);
    const std::vector<float> FOI_hits_DX = buffered_stream.fill_vector_float<float>(FOI_hits_N);
    const std::vector<float> FOI_hits_DY = buffered_stream.fill_vector_float<float>(FOI_hits_N);
    buffered_stream.skip_record(DELIMITER);
    const std::vector<float> FOI_hits_T = buffered_stream.fill_vector_float<float>(FOI_hits_N);
    buffered_stream.skip_record(DELIMITER);
    const std::vector<size_t> FOI_hits_S = \
        buffered_stream.fill_vector_uint<size_t>(FOI_hits_N);

    buffered_stream.fill_iterator_float<std::vector<float>::iterator>(
        result->begin() + N_RAW_FEATURES - N_RAW_FEATURES_TAIL, N_RAW_FEATURES_TAIL);

    // (*result)[ind] = feat
    (*result)[65] = (*result)[NSHARED_INDEX] / (*result)[NDOF_INDEX];
    (*result)[66] = (*result)[PT_INDEX] / (*result)[P_INDEX];
    (*result)[67] = (*result)[PT_INDEX] * (*result)[P_INDEX];

    for (size_t station = 0; station < N_STATIONS; ++station) {
        (*result)[68 + 2 * station] = (*result)[NCL_INDEX + station] / ((*result)[PT_INDEX] + 1);
        (*result)[68 + 2 * station + 1] = (*result)[NCL_INDEX + station] / ((*result)[P_INDEX] + 1);
    }

    for (size_t station = 0; station < N_STATIONS; ++station) {
        (*result)[76 + 2 * station] = (*result)[AVG_CS_INDEX + station] / ((*result)[PT_INDEX] + 1);
        (*result)[76 + 2 * station + 1] = (*result)[AVG_CS_INDEX + station] / ((*result)[P_INDEX] + 1);
    }

    (*result)[84] = (*result)[AVG_CS_INDEX + 0] / (1 + (*result)[AVG_CS_INDEX + 1]);
    (*result)[85] = (*result)[AVG_CS_INDEX + 0] / (1 + (*result)[AVG_CS_INDEX + 2]);
    (*result)[86] = (*result)[AVG_CS_INDEX + 0] / (1 + (*result)[AVG_CS_INDEX + 3]);
    (*result)[87] = (*result)[AVG_CS_INDEX + 1] / (1 + (*result)[AVG_CS_INDEX + 2]);
    (*result)[88] = (*result)[AVG_CS_INDEX + 1] / (1 + (*result)[AVG_CS_INDEX + 3]);
    (*result)[89] = (*result)[AVG_CS_INDEX + 2] / (1 + (*result)[AVG_CS_INDEX + 3]);

    (*result)[84 + 6] = (*result)[NCL_INDEX + 0] / (1 + (*result)[NCL_INDEX + 1]);
    (*result)[85 + 6] = (*result)[NCL_INDEX + 0] / (1 + (*result)[NCL_INDEX + 2]);
    (*result)[86 + 6] = (*result)[NCL_INDEX + 0] / (1 + (*result)[NCL_INDEX + 3]);
    (*result)[87 + 6] = (*result)[NCL_INDEX + 1] / (1 + (*result)[NCL_INDEX + 2]);
    (*result)[88 + 6] = (*result)[NCL_INDEX + 1] / (1 + (*result)[NCL_INDEX + 3]);
    (*result)[89 + 6] = (*result)[NCL_INDEX + 2] / (1 + (*result)[NCL_INDEX + 3]);

    // 96
    (*result)[96 + 0] = (*result)[AVG_CS_INDEX + 0] / (1 + (*result)[NCL_INDEX + 1]);
    (*result)[96 + 1] = (*result)[AVG_CS_INDEX + 0] * (*result)[NCL_INDEX + 1];

    (*result)[98 + 0] = (*result)[AVG_CS_INDEX + 0] / (1 + (*result)[NCL_INDEX + 2]);
    (*result)[98 + 1] = (*result)[AVG_CS_INDEX + 0] * (*result)[NCL_INDEX + 2];

    (*result)[100 + 0] = (*result)[AVG_CS_INDEX + 0] / (1 + (*result)[NCL_INDEX + 3]);
    (*result)[100 + 1] = (*result)[AVG_CS_INDEX + 0] * (*result)[NCL_INDEX + 3];

    (*result)[102 + 0] = (*result)[AVG_CS_INDEX + 1] / (1 + (*result)[NCL_INDEX + 2]);
    (*result)[102 + 1] = (*result)[AVG_CS_INDEX + 1] * (*result)[NCL_INDEX + 2];

    (*result)[104 + 0] = (*result)[AVG_CS_INDEX + 1] / (1 + (*result)[NCL_INDEX + 3]);
    (*result)[104 + 1] = (*result)[AVG_CS_INDEX + 1] * (*result)[NCL_INDEX + 3];

    (*result)[106 + 0] = (*result)[AVG_CS_INDEX + 2] / (1 + (*result)[NCL_INDEX + 3]);
    (*result)[106 + 1] = (*result)[AVG_CS_INDEX + 2] * (*result)[NCL_INDEX + 3];

    // 108
    for (size_t station = 0; station < N_STATIONS; ++station) {
        (*result)[108 + 2 * station] = (*result)[MATCHEDHIT_X_INDEX + station] - (*result)[LEXTRA_X_INDEX + station];
        if ((*result)[108 + 2 * station] > 0){
        (*result)[108 + 2 * station + 1] = (*result)[108 + 2 * station];
        } else{
        (*result)[108 + 2 * station + 1] = (*result)[108 + 2 * station] * -1;
        }
    }
    // 116
    for (size_t station = 0; station < N_STATIONS; ++station) {
        (*result)[116 + 2 * station] = (*result)[MATCHEDHIT_Y_INDEX + station] - (*result)[LEXTRA_Y_INDEX + station];
        if ((*result)[116 + 2 * station] > 0){
        (*result)[116 + 2 * station + 1] = (*result)[116 + 2 * station];
        } else{
        (*result)[116 + 2 * station + 1] = (*result)[116 + 2 * station] * -1;
        }
    }
}
