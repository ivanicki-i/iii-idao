ZIP_NAME=cpp_serious_model1_20190210_try1_w.zip && \
DEST_DIR=/Users/iiivanitskiy/main/idao_task_c && \
rm -rf $DEST_DIR && \
mkdir $DEST_DIR && \
unzip $ZIP_NAME -d $DEST_DIR && \
cp -r *.cpp $DEST_DIR && \
cp -r *.h $DEST_DIR && \
cd $DEST_DIR && \
rm -f *.zip && \
rm -rf .ipynb_checkpoints && \
rm -rf __pycache__ && \
touch -A -120000 ./* && \
zip -r $ZIP_NAME ./ && \
touch -A -050000 ./* && \
ls -lh .